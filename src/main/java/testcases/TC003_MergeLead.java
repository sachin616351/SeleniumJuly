		package testcases;
		
		import org.openqa.selenium.WebElement;
		import org.testng.annotations.Test;
		
		import wdMethods.SeMethods;
		
		public class TC003_MergeLead extends SeMethods {
			@Test
			
			public void MergeLead() {
				
				startApp("chrome", "http://leaftaps.com/opentaps");
				WebElement eleUserName = locateElement("id", "username");
				type(eleUserName, "DemoSalesManager");
				WebElement elePassword = locateElement("id","password");
				type(elePassword, "crmsfa");
				WebElement eleLogin = locateElement("class","decorativeSubmit");
				click(eleLogin);
				WebElement elecrmsfa= locateElement("linktext", "CRM/SFA");
				click(elecrmsfa);
				WebElement elecreat= locateElement("linktext", "Create Lead");
				click(elecreat);
				WebElement elecmerge= locateElement("linktext", "Merge Leads");
				click(elecmerge);
				
				WebElement elefromlead = locateElement("id", "ComboBox_partyIdFrom");
				type(elefromlead, "sac");
				
				
				
			}
		
		}
