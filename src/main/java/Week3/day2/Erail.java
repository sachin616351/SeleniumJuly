package Week3.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Erail {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
  
		System.setProperty("webdriver.chrome.driver","./drivers/Chromedriver.exe");
		//Create Obj
		ChromeDriver driver = new ChromeDriver();
		//Hit the url
		driver.get("https://erail.in/");
		//Maximize the screen
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//from Station
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		//To Station
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("SBC",Keys.TAB);
		//for chk box
		boolean selected = driver.findElementById("chkSelectDateOnly").isSelected();
		if (selected) {
			driver.findElementById("chkSelectDateOnly").click();
		}
		
		List<WebElement> findElementByXPath = (List<WebElement>) driver.findElementByXPath("//button[text()='MFP YPR EXP']");
		for(WebElement Eachoption: findElementByXPath) {
			System.out.println(((WebElement) findElementByXPath).getText());}
		}
		
		
		
	}