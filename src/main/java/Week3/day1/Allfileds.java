package Week3.day1;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Allfileds {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		//create object
		ChromeDriver driver = new ChromeDriver();
		//load the URL
		driver.get("http://leaftaps.com/opentaps/");
		//enter username
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//enter password
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("CTS");
		driver.findElementById("createLeadForm_firstName").sendKeys("Sachin");
		driver.findElementById("createLeadForm_lastName").sendKeys("Ledange");
		WebElement src = driver.findElementById("createLeadForm_dataSourceId");
		Select dd =new Select(src);
		dd.selectByVisibleText("Employee");
		WebElement src1 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dd1 = new Select (src1);
		dd1.selectByValue("CATRQ_CARNDRIVER");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Som");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Dev");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("HI");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Selenium Traning");
		driver.findElementById("createLeadForm_departmentName").sendKeys("RCG");
		driver.findElementById("createLeadForm_departmentName").sendKeys("	Retail");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("10000000");
		WebElement src3 = driver.findElementById("createLeadForm_currencyUomId");
		Select dd3 =new Select(src3);
		dd3.selectByIndex(2);
		WebElement src2 = driver.findElementById("createLeadForm_industryEnumId");
		Select dd2 =new Select(src2);
		dd2.selectByVisibleText("Computer Software");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("400");
		WebElement src4 = driver.findElementById("createLeadForm_ownershipEnumId");
		Select dd4 =new Select(src4);
		dd4.selectByVisibleText("Corporation");
		driver.findElementById("createLeadForm_sicCode").sendKeys("25000");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("Green");
		driver.findElementById("createLeadForm_description").sendKeys("Selenium is work");
		driver.findElementById("createLeadForm_importantNote").sendKeys("Prepare well");
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("5");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9403705698");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("07239");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("jai");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("sachinledange007@gmail.com");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("5555");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.cts.com");
		driver.findElementById("createLeadForm_generalToName").sendKeys("Rohit");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("Som");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("Wani,Jijau nagar");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("Nagpur,Maharashtra");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("Som");
		driver.findElementById("createLeadForm_generalCity").sendKeys("Nagpur");
		WebElement src5 = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select dd5 =new Select(src5);
		dd5.selectByVisibleText("Virginia");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("7");
		WebElement src6 = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select dd6 =new Select(src6);
		dd6.selectByVisibleText("India");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("445304");
		driver.findElementByClassName("smallSubmit").click();
	}
		}
