package Week4.day1;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class learnMouseAction {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.get("http://jqueryui.com/draggable/");
		driver.switchTo().frame(0);
		WebElement drag=driver.findElementByXPath("//div[@id='draggable']/p");
		Actions builder=new Actions(driver);
		builder.dragAndDropBy(drag, 100, 100).perform();
		WebElement text = driver.findElementByXPath("//div[@id='draggable']/p");
		String text2 = text.getText();
		System.out.println(text2);
	    
		

	}

}
