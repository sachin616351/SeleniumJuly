package Week4.day1;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;


public class learnwindow1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//from chella
		
		System.setProperty("webdriver.chrome.driver","./drivers/Chromedriver.exe");
		//Create Obj
		ChromeDriver driver = new ChromeDriver();
		//Hit the url
		driver.get("https://www.irctc.co.in/eticketing/loginHome.jsf");
		//Maximize the screen
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByLinkText( "AGENT LOGIN" ).click();
		//clicking on contact us
		driver.findElementByLinkText("Contact Us").click();		
		//finding the number of windows open
		Set<String> w1 = driver.getWindowHandles();
		System.out.println(w1.size());
		// to do get method method converting to LIST type
		List<String> w2 = new ArrayList<String>();
		w2.addAll(w1);
		//move the control to second window
		String secondwindow = w2.get(1);
		driver.switchTo().window(secondwindow);
		//print the title
		String secondwindowtitle = driver.getTitle();
		System.out.println(secondwindowtitle);
		//print the url
		String secondwindowURL = driver.getCurrentUrl();
		System.out.println(secondwindowURL);
		driver.quit();

		
		

	}

}
