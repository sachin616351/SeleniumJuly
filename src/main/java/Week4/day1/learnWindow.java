package Week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class learnWindow {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		//Will get window ID
		String windowHandle=driver.getWindowHandle();
		System.out.println(windowHandle);
		//find the number of window open
		Set<String> allwindows=driver.getWindowHandles();
		System.out.println(allwindows.size());
		driver.findElementByLinkText("Contact Us").click();
		//find the number of window open(2nd window)
		Set<String> allwindows1=driver.getWindowHandles();
		System.out.println(allwindows1.size());
		String title=driver.getTitle();
		System.out.println(title);
		//to get control of second window
		List<String> listofWindows=new ArrayList<String>();//add all windows in a list
		listofWindows.addAll(allwindows1);//take all content
		String secondwindow=listofWindows.get(1); //2nd window  index 1
		driver.switchTo().window(secondwindow);
		
		String windowHandle1=driver.getWindowHandle();
		System.out.println(windowHandle1);
		
		
		String newtitle=driver.getTitle();
		System.out.println(newtitle);
		driver.quit();

	}

}
