package Week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class homeMergelead {

	public static void main(String[] args) throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementByLinkText("Merge Leads").click();
		
		String windowHandle=driver.getWindowHandle();
		System.out.println(windowHandle);
		
		Set<String> allwindows=driver.getWindowHandles();
		System.out.println(allwindows.size());
		
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[1]").click();
		
		Set<String> allwindows1=driver.getWindowHandles();
		System.out.println(allwindows1.size());
		
		//to get control of second window
				List<String> listofWindows=new ArrayList<String>();//add all windows in a list
				listofWindows.addAll(allwindows1);//take all content
				String secondwindow=listofWindows.get(1); //2nd window  index 1
				driver.switchTo().window(secondwindow);
				
				driver.findElementByXPath("//input[@name='id']").sendKeys("1003");
				driver.findElementByXPath("//button[text()='Find Leads']").click();
				//driver.findElementByXPath("//a[text()='Suganya']").click();
		driver.close();
		//to get control of first window
		List<String> listofWindows1=new ArrayList<String>();//add all windows in a list
		listofWindows1.addAll(allwindows);//take all content
		String firstwindow=listofWindows1.get(0); //2nd window  index 1
		driver.switchTo().window(firstwindow);
		driver.findElementByClassName("buttonDangerous").click();
		driver.switchTo().alert().accept();
		
		driver.findElementByXPath("//input[@id='ComboBox_partyIdFrom']").sendKeys("sachin");
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		Thread.sleep(5000);
		
	File source = driver.getScreenshotAs(OutputType.FILE);
		File dest = new File("./snaps/img.jpg");
		FileUtils.copyFile(source,dest);

	}

}
