package Week6.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcelIntegrate {

	public static Object[][] getexcelData(String fileName) throws IOException {
		// TODO Auto-generated method stub

		//Locateworkbook
		XSSFWorkbook wbook=new XSSFWorkbook("./data/"+fileName+".xlsx");
		
		//getsheet  //always read data from sheet 1
		
	//	XSSFSheet sheet = wbook.getSheet("creatleadsheet");
		XSSFSheet sheet= wbook.getSheetAt(0);
		//get last row number
		
		int lastRowNum = sheet.getLastRowNum();
		//get last column number
		
		short lastCellNum = sheet.getRow(0).getLastCellNum();
		
		Object[][] data= new Object [lastRowNum][lastCellNum];  //creat object array
		
		for(int j=1;j<=lastRowNum;j++)
		{
			XSSFRow row = sheet.getRow(j);
			
			for(int i=0;i<lastCellNum;i++) {
				XSSFCell cell = row.getCell(i);
				
				String stringCellValue = cell.getStringCellValue();
				
				data[j-1][i]= stringCellValue;  //excel values should store in data
				System.out.println(stringCellValue);

			}

		}
		wbook.close();
		return data;
}
	
	
}
		
	


