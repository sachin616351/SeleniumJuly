package Week6.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
//ExtendProjectMethod
public class TC002_CreateLead extends ProjectMeth{
	@BeforeClass
	public void setdata() {
		testcaseName="TC002 CREAT LEAD";
		testcaseDesc="CREATLEAD";
		Author="Sachin";
		Category="Regression";
		excelfileName= "CreateLead";
	}


	//@BeforeMethod
	@Test(dataProvider = "Fetchdata")
	//@Test(invocationCount=2, timeOut=30000)
	public void createLead(String Cname, String Fname, String Lname, String Email, String Phnumb) {

		//	login();
		WebElement elecreat= locateElement("linktext", "Create Lead");
		click(elecreat);
		WebElement elecompanyname= locateElement("id", "createLeadForm_companyName");
		type(elecompanyname, Cname);

		WebElement elefirstname= locateElement("id", "createLeadForm_firstName");
		type(elefirstname,Fname );

		WebElement elelastname= locateElement("id", "createLeadForm_lastName");
		type(elelastname, Lname);

		WebElement eleemail= locateElement("xpath", "(//input[@name='primaryEmail'])[4]");
		type(eleemail, Email);


		WebElement eleephone= locateElement("xpath","(//input[@name='primaryPhoneNumber'])[4]");
		type(eleephone,Phnumb);

		WebElement elecurrency= locateElement("id", "createLeadForm_currencyUomId");
		click(elecurrency);
		selectDropDownUsingText(elecurrency,"INR - Indian Rupee");


		WebElement elesubmit= locateElement("class", "smallSubmit");
		click(elesubmit);


	}
//	@DataProvider(name = "createLead")
//		public Object[][] getData(){
//
//			Object[][] data = new Object[2][5];
//			data[0][0] = "wipro";
//			data[0][1] = "sachin";
//			data[0][2] = "le";
//		data[0][3] = "re@.com";
//		data[0][4] = "613";
//
//			data[1][0] = "infosys";
//			data[1][1] = "Chella";
//			data[1][2] = "tl";
//			data[1][3] = "cam@.com";
//			data[1][4] = "234";
//
//			return data;

	//}
	



	

}










