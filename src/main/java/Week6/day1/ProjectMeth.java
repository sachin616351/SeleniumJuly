package Week6.day1;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Week6.day2.ReadExcel;
import Week6.day2.ReadExcelIntegrate;
import wdMethods.SeMethods;

public class ProjectMeth extends SeMethods {
	
	@DataProvider(name="Fetchdata")
	public Object[][] getdata() throws IOException {
		
		Object[][] excelData= ReadExcelIntegrate.getexcelData(excelfileName);
		
		return excelData;
	}
	
	
	
	@BeforeMethod
	@Parameters({"browser","appurl","username","password"})
	public void login(String browserName,String URL,String Uname, String PWD)
	
	{
		startApp(browserName,URL);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName,Uname);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, PWD);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement elecrmsfa= locateElement("linktext", "CRM/SFA");
		click(elecrmsfa);
	}
	
	@AfterMethod
	public void close() {
		
		closeAllBrowsers();
		
	}
	

}
