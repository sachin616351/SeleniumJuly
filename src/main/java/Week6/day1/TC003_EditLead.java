package Week6.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import week5day1.ProjectMethods;

public class TC003_EditLead extends ProjectMeth{
	
	@BeforeClass
	public void setdata() {
		testcaseName="TC002 CREAT LEAD";
		 testcaseDesc="CREATLEAD";
	      Author="	Dinesh";
		 Category="sanity";
	}
	
	
	
     @Test(dependsOnMethods="Week6.day1.TC002_CreateLead.createLead")
	public void editlead() throws InterruptedException {

		//login();
		WebElement leadmenu = locateElement("linktext", "Leads");
		click(leadmenu);
		WebElement findlead = locateElement("xpath", "//a[text() = 'Find Leads']");
		click(findlead);
		WebElement leadname = locateElement("xpath", "(//input[@name = 'firstName'])[3]");
		type(leadname, "Dinesh");
		WebElement filtrlead = locateElement("xpath", "//button[text() = 'Find Leads']");
		click(filtrlead);
		Thread.sleep(5000);
		WebElement pickalead = locateElement("xpath", "//td[@class = 'x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a");
		click(pickalead);
		String expectedTitle = driver.getTitle();
		verifyTitle(expectedTitle);
		WebElement editlink = locateElement("linktext", "Edit");
		click(editlink);
		WebElement updtcompname = locateElement("id", "updateLeadForm_companyName");
		updtcompname.clear();
		String text = "newcompanyname1";
		type(updtcompname, text);
		WebElement updatelead = locateElement("xpath", "//input[@value = 'Update']");
		click(updatelead);
		WebElement updatedcmpnyname = locateElement("id", "viewLead_companyName_sp");
		String Actualcmpnyname = updatedcmpnyname.getText();
		//Verify the Expected & Actual
		if(Actualcmpnyname.contains(text))
		{
			System.out.println("Verification of Company Name is Success");
		}else
			System.out.println("Verification of Company Name is Failed");
		//closeBrowser();
	}
}