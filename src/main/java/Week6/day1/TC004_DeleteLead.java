package Week6.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import week5day1.ProjectMethods;

public class TC004_DeleteLead extends ProjectMethods {
	
	
	@BeforeClass
	public void setdata() {
		testcaseName="TC002 CREAT LEAD";
		 testcaseDesc="CREATLEAD";
	      Author="Sachin";
		 Category="Regression";
	}
	

	@Test(dependsOnMethods="Week6.day1.TC002_CreateLead.createLead")
	public void deleteLead() throws InterruptedException {
	//	login();
		//WebElement crmlink = locateElement("linktext", "CRM/SFA");
	//	click(crmlink);
		WebElement leadmenu = locateElement("xpath","//a[text()='Leads']");
		click(leadmenu);
		WebElement findlead = locateElement("xpath", "//a[text() = 'Find Leads']");
		click(findlead);
		WebElement tapPhnTab = locateElement("xpath", "(//a[@class = 'x-tab-right'])[2]");
		click(tapPhnTab);
		WebElement PhnAreaCode = locateElement("xpath", "//input[@name = 'phoneAreaCode']");
		type(PhnAreaCode, "01");
		WebElement PhnNmbr = locateElement("xpath", "//input[@name = 'phoneNumber']");
		type(PhnNmbr, "9952525425");
		WebElement filtrlead = locateElement("xpath", "//button[text() = 'Find Leads']");
		click(filtrlead);
		Thread.sleep(5000);
		WebElement pickalead = locateElement("xpath", "//td[@class = 'x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a");
		String getID = pickalead.getText();
		click(pickalead);
		WebElement deletelink = locateElement("linktext", "Delete");
		click(deletelink);
		WebElement findDeletedLead = locateElement("xpath", "//a[text() = 'Find Leads']");
		click(findDeletedLead);
		WebElement LeadID = locateElement("xpath", "//input[@name = 'id']");
		type(LeadID, getID);
		WebElement filtrleadagain = locateElement("xpath", "//button[text() = 'Find Leads']");
		click(filtrleadagain);
	//	closeBrowser();
		
		
		
		
	}
}
