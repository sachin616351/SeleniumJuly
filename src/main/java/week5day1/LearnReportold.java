package week5day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReportold {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		//create uneditable html file
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/report.html");
	//history
		html.setAppendExisting(true); 
		
		//editable
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		
		//inputs
		
		ExtentTest test= extent.createTest("TC002_CreatLead","Creat") ;
		test.assignAuthor("Sachin");
		test.assignCategory("Smoke");
		test.pass("The data demosalesManager entered succsessfully",MediaEntityBuilder.createScreenCaptureFromPath("./../Snaps/img1.png").build());
		test.pass("The data crmsfa is entered succsss");
		test.pass("Login buyyon clicked suucessfully");
		
		//flush erase memory
		extent.flush();
		
		
		
		
	}

}
