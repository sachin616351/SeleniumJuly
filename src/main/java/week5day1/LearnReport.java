package week5day1;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReport {
	
	public static ExtentReports extent;
	public static ExtentTest test;
	public  String testcaseName, testcaseDesc, Author, Category,excelfileName ;
	
	
	
	@BeforeSuite
	public void startResult()
	{
		//create uneditable html file
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/report.html");
	//history
		html.setAppendExisting(true); 
		
		//editable
		extent = new ExtentReports();
		extent.attachReporter(html);

	}
	
	@BeforeMethod
	public void startTestCase()
	{
 test = extent.createTest(testcaseName,testcaseDesc) ;
		test.assignAuthor(Author);
		test.assignCategory(Category);
	}
	
	
	
	
	public void reportstep(String desc,String status) {
		
		if (status.equalsIgnoreCase("pass")) {
			test.pass(desc);
			
		}if (status.equalsIgnoreCase("fail")) {
			test.fail(desc);
			
		}
	}
	@AfterSuite
	public void flush() {
		extent.flush();
	}
	
}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	


