package week5day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import wdMethods.SeMethods;

public class ProjectMethods extends SeMethods {
	@BeforeMethod
	@Parameters({"browser","appurl","username","password"})
	public void login(String browserName, String Uname, String URL, String PWD)
	
	{
		startApp(browserName,URL);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName,Uname);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, PWD);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement elecrmsfa= locateElement("linktext", "CRM/SFA");
		click(elecrmsfa);
	}
	
	@AfterMethod
	public void close() {
		
		closeAllBrowsers();
		
	}
	

}