package projectDay;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class FixMyBugs_solu {

	@Test
	public void sample() throws InterruptedException {
//	public void main(String args) throws InterruptedException {

		// launch the browser
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.myntra.com/");

		// Mouse Over on Men
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElementByLinkText("Men")).perform();

		// Click on Jackets
		driver.findElementByXPath("//a[@href='/men-jackets']").click();


		// Find the count of Jackets
		String leftCount = 
				driver.findElementByXPath("//input[@value='Jackets']/following::span")
				.getText()
				.replaceAll("\\D", "");
		System.out.println(leftCount);


		// Click on Jackets and confirm the count is same
		driver.findElementByXPath("//label[text()='Jackets']").click();

		// Wait for some time
		Thread.sleep(5000);

		// Check the count
		String rightCount = 
				driver.findElementByXPath("//h1[text()='Mens Jackets']/following::span")
				.getText()
				.replaceAll("\\D", "");
		System.out.println(rightCount);

		// If both count matches, say success
		if(leftCount.equals(rightCount)) {
			System.out.println("The count matches on either case");
		}else {
			System.err.println("The count does not match");
		}

		// Click on Offers
		driver.findElementByXPath("//h4[text()='Offers']").click();

		// Find the costliest Jacket
		List<WebElement> productsPrice = driver.findElementsByXPath("//span[@class='product-discountedPrice']");
		List<String> onlyPrice = new ArrayList<String>();

		for (WebElement productPrice : productsPrice) {
			onlyPrice.add(productPrice.getText().replaceAll("\\D", ""));
		}

		// Sort them 
		String max = Collections.max(onlyPrice);

		// Find the top one
		System.out.println(max);
		

		// Print Only Allen Solly Brand Minimum Price
		driver.findElementByXPath("//div[@class = 'brand-more']").click();
		Thread.sleep(5000);
		driver.findElementByXPath("(//ul[@class = 'FilterDirectory-list'])/li[4]/label").click();
		//Wait for some time
		Thread.sleep(5000);
		driver.findElementByXPath("//span[@class = 'myntraweb-sprite FilterDirectory-close sprites-remove']").click();
		//Min Price
		List<WebElement> allenPrice = driver.findElementsByXPath("//span[@class='product-discountedPrice']");
		List<String> sollyPrice = new ArrayList<String>();

		for (WebElement prodPrice : allenPrice) {
			sollyPrice.add(prodPrice.getText().replaceAll("\\D", ""));
		}
		
		// Sort them 
		String min = Collections.min(sollyPrice);

		// Find the top one
		System.out.println("Allen Solly Min Price: "+min);
		
		// Find the costliest Jacket
		// Sort them 
		String ASMAX = Collections.max(sollyPrice);

		// Find the top one
		System.out.println("Allen Solly Max Price: "+ASMAX);
		
		

	}

}