package projectDay;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.math3.analysis.function.Max;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class Zoomcar {
	private static final int List = 0;

	public static  void main(String args[]) throws InterruptedException {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				ChromeDriver driver=new ChromeDriver();
				driver.manage().window().maximize();
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				driver.get("https://www.zoomcar.com/chennai/");
				driver.findElementByXPath("//a[text()='Start your wonderful journey']").click();
				try {
					driver.findElementByXPath("//span[text()='Retry']").click();
					Thread.sleep(15000);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					System.out.println("Retry not appeared");
				}
				
				try {
					WebElement searcherror = driver.findElementByXPath("//input[@class='search']");
					searcherror.sendKeys("taramani");
					Thread.sleep(3000);
					searcherror.sendKeys(Keys.DOWN,Keys.ENTER);
				} catch (Exception e) {
					// TODO Auto-generated catch block
				System.err.println("Element not found");
				}
				Thread.sleep(3000);
				driver.findElementByXPath("//button[@class='proceed']").click();
				driver.findElementByXPath("//div[text()='Mon']").click();
				driver.findElementByXPath("//button[text()='Next']").click();
				driver.findElementByXPath("//button[text()='Done']").click();
				
				List<WebElement> prices = driver.findElementsByXPath("(//div[@class='price'])");
				List<Integer> pricesnew =new ArrayList<>();
				for (WebElement eachprice : prices) {
					String replacedstring =eachprice.getText().replaceAll("\\D", "");
					int parseInt = Integer.parseInt(replacedstring);
					pricesnew.add(parseInt);
				}
				//getting the maximum value
				Integer max = Collections.max(pricesnew);
				System.out.println("max "+max);
				//Collections.sort(pricesnew);
				//System.out.println(pricesnew.get(pricesnew.size()-1));
				String text = driver.findElementByXPath("//*[contains(text(),'"+ max +"')]/preceding::h3[1]").getText();
				System.out.println(text);
}
}
