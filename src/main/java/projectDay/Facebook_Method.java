package projectDay;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import week4day2.seMthd;

public class Facebook_Method extends seMthd {
	//@Test
	@BeforeMethod
	void login() {
		
		startApp("chrome", "https://www.facebook.com/");
		WebElement eleUserName = locateElement("id", "email");
		type(eleUserName, "sachinledange007@gmail.com");
		
		
		WebElement elePassword = locateElement("id","pass");
		type(elePassword, "sam@123");
		
		WebElement eleLogin = locateElement("xpath"," //input[@type='submit']");
		click(eleLogin);
	}
@AfterSuite
public void close() {
		
		closeAllBrowsers();
		
	}
	
}
